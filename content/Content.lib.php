<?php
class Content {

	public function __construct() {		
		return true;		
	}

	public function getContent(){
		$opts = array('http' =>
		    array(
		        'header'  => 'User-agent: Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3',
		    )
		);
		$context  = stream_context_create($opts);
		$this->html = file_get_contents($this->url, false, $context);
	}

	public function getJson($key,$single=false){	
		$doc = new DOMDocument();
		$load = @$doc->loadHTML($this->html);
		$xpath = new DOMXPath($doc);
		$this->result = $xpath->query("//".$key."");
		#$doc->saveHTML();
		$list = array();
		if(!empty($this->result)):
			foreach ($this->result as $n){
				$value = $doc->saveHTML($n);
				$list[] = $value;
			}		
		endif;
		if($single==false):
			$this->list = $list;
		else:
			$this->list = $list[0];
		endif;

		return $this->list;
	}

	public function setPublisher($publisher){
		$this->publisher 		= $publisher;
		$this->method_callback($this->publisher);
	}

	public function setSource(){		
		$this->source 			= $this->publisher;
	}

	public function setUrl($url){
		$this->url = $url;
		$this->getContent();
	}

	public function setLimit($limit){
		if($limit>0){
			$this->limit = $limit;
		}
	}

	public function getMetaTags(){
		$metaname 			= $this->getJson("meta");
		$this->meta 		= (object)array();
		if(!empty($metaname)):
			foreach($metaname as $k=>$v):
				$this->meta->{$this->getBetween($v,'"','"')} 
					= strip_tags($this->getBetween($v,'content="','"'));
			endforeach;
		endif;
	}

	public function removeNewlines($input){		
		return str_replace(array("\t","\n","\r","\x20\x20","\0","\x0B"), "", html_entity_decode($input));		
	}

	public function getBetween($string, $start, $end){
		$string = $this->removeNewlines($string);
	    $string = " ".$string;
	    $ini = strpos($string,$start);
	    if ($ini == 0) return "";
	    $ini += strlen($start);
	    $len = strpos($string,$end,$ini) - $ini;
	    return substr($string,$ini,$len);
	}

	public function deleteBetween($string, $start, $end ) {
		$string = $this->removeNewlines($string);
		$beginningPos = strpos($string, $start);
		$endPos = strpos($string, $end);
		if ($beginningPos === false || $endPos === false) {
			return $string;
		}

		$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

		return str_replace($textToDelete, '', $string);
	}

	public function method_callback(){
		if (method_exists($this, $this->publisher)){
			return $this->{$this->publisher}();
		} 
	}

	public function debugArray($array){
		echo '<pre/>'; exit(print_r($array));
	}

	public function Kompas(){		
		$this->setUrl('http://www.kompas.com');
		$this->setSource();

		//get kompas list latest update
		$this->getJson("li/div[@class='ListCol']/div/a/@href");
		if(empty($this->limit)):
			$this->limit = count($this->list);
		endif;

		$a = (object)array();
		if(count($this->list)){
			$i = 0;	
			foreach ($this->list as $k => $v) {
				if($k<$this->limit):
					$this->setPublisher('detail_article');

					$this->siteurl = $this->getBetween($v, 'href="', '"');
					$this->setUrl($this->siteurl);

					$this->headline = $this->getJson("div[@class='headline column']/a/text()", true);
					$this->img = $this->getJson("div[@class='imgHL']/img/@src", true);
					$this->body = $this->getJson("div[@class='contentArticle']",true);
					$this->categories = $this->getJson("div[@class='logo2 left']/h1/text()",true);
					$this->date = $this->getJson("div[@class='headline column']/h6/text()", true);
					$this->method_callback($this->publisher);			

					if($this->headline && $this->body):
						$a->{$i}		= $this->json;
					endif;
					#echo $this->html; exit();
					$i++;
				endif;
			}
		}
		$this->return = $a;
		return true;
	}

	public function Detik(){		
		$this->setUrl('http://www.detik.com');
		$this->setSource();
		
		//duplicate original html
		$html = $this->html;
		//get detik latest update
		$this->html = $this->getJson("ul[@class='list_1']", true);
		//get list latest
		$this->getJson("li/h3/a/@href");
		if(empty($this->limit)):
			$this->limit = count($this->list);
		endif;

		$a = (object)array();
		if(count($this->list)){
			$i = 0;	
			foreach ($this->list as $k => $v) {
				if($k<$this->limit):
					$this->setPublisher('detail_article');

					$this->siteurl = $this->getBetween($v, 'href="', '"');
					$this->setUrl($this->siteurl);
					
					$this->headline = $this->getJson("div[@class='detail']/h1/text()", true);
					$this->img = $this->getJson("div[@class='detail']/div[@class='pic']/img/@src", true);
					$this->body = $this->getJson("div[@class='detail']/div[@class='text_detail']",true);
					$this->categories = $this->getJson("div[@class='breadcrumb']/a/text()",true);
					$this->date = $this->getJson("div[@class='detail']/div[@class='date']/text()", true);
					$this->author = $this->getJson("div[@class='detail']/div[@class='author']/strong/text()", true);
					$this->method_callback($this->publisher);			
				
					if($this->headline && $this->body):
						$a->{$i}		= $this->json;
					endif;
					$i++;
				endif;
			}
		}
		$this->return = $a;
		return true;
	}

	public function Merdeka(){		
		$this->setUrl('http://m.merdeka.com');
		$this->setSource();
		
		//duplicate original html
		$html = $this->html;
		//decode gzdecode
		if(strpos($this->html, '<html')===false):
			$this->html = gzdecode($html);
		endif;
		//get merdeka latest update
		$this->html = $this->getJson("div[@id='mdk-news-top']", true);
		if(empty($this->list)):
			$this->html = $html;
			$this->html = $this->getJson("div[@id='mdk-news-top']", true);
		endif;
		//get list latest
		$this->getJson("a/@href");
		if($this->list):
			$this->list = array_unique($this->list);
			$this->list = array_values($this->list);
		endif;

		if(empty($this->limit)):
			$this->limit = count($this->list);
		endif;
		$a = (object)array();
		if(count($this->list)){
			$i = 0;	
			foreach ($this->list as $k => $v) {
				if($k<$this->limit):
					$this->setPublisher('detail_article');

					$this->siteurl = "http://m.merdeka.com".$this->getBetween($v, 'href="', '"');
					$this->setUrl($this->siteurl);
					//duplicate original html
					$html = $this->html;
					//decode gzdecode
					if(strpos($this->html, '<html')===false):
						$this->html = gzdecode($html);
					endif;

					//get merdeka title
					$this->headline = $this->getJson("span[@id='mdk-news-title']/text()", true);
					if(empty($this->list)):
						$this->html = $html;
						$this->headline = $this->getJson("span[@id='mdk-news-title']/text()", true);
					endif;

					$this->img = $this->getJson("div[@id='detail_news']/img/@src", true);
					$this->body = '<p><span>'.$this->getBetween($this->html, '<p><span class="first-p">','</p>').'</p>';
					$this->categories = $this->getJson("div[@id='mdk-rubric-top']/a[2]/text()",true);
					$this->date = $this->getJson("span[@class='mdk-det-time']/text()", true);
					$this->author = $this->getJson("span[@class='mdk-reporter']/text()", true);
					$this->method_callback($this->publisher);			
				
					if($this->headline && $this->body):
						$a->{$i}		= $this->json;
					endif;

					$i++;
				endif;
			}
		}
		$this->return = $a;
		return true;
	}

	public function Liputan6(){		
		$this->setUrl('http://m.liputan6.com/indeks/terkini');
		$this->setSource();
		
		//duplicate original html
		$html = $this->html;
		//get liputan 6 list latest update
		$this->getJson("div[@class='figure-thumb']/a/@href");
		if(empty($this->limit)):
			$this->limit = count($this->list);
		endif;

		$a = (object)array();
		if(count($this->list)){
			$i = 0;	
			foreach ($this->list as $k => $v) {
				if($k<$this->limit):
					$this->setPublisher('detail_article');

					$this->siteurl = $this->getBetween($v, 'href="', '"');
					$this->setUrl($this->siteurl);
					
					$this->headline = $this->getJson("div[@class='entry-header']/h1/text()", true);
					$this->img = $this->getJson("div[@class='gallery-images']/div/@data-image", true);
					$this->body = $this->getJson("div[@class='text-detail']",true);
					$this->categories = $this->getJson("li[@typeof='v:Breadcrumb']/a/text()");
					$this->date = $this->getJson("span[@class='by-author']/span[@class='datetime']/text()", true).$this->getJson("span[@class='by-author']/span[@class='datetime']/span/text()", true);
					$this->author = $this->getJson("span[@class='by-author']/span/a/text()", true);
					$this->method_callback($this->publisher);			
				
					if($this->headline && $this->body):
						$a->{$i}		= $this->json;
					endif;

					$i++;
				endif;
			}
		}
		$this->return = $a;
		return true;
	}

	public function Viva(){		
		$this->setUrl('http://m.news.viva.co.id/indeks');
		$this->setSource();
		
		//duplicate original html
		$html = $this->html;
		//get Viva list latest update
		$this->getJson("div/a[@class='title-other']/@href");
		if(empty($this->limit)):
			$this->limit = count($this->list);
		endif;
		#$this->debugArray($this->list);
		$a = (object)array();
		if(count($this->list)){
			$i = 0;	
			foreach ($this->list as $k => $v) {
				if($k<$this->limit):
					$this->setPublisher('detail_article');

					$this->siteurl = $this->getBetween($v, 'href="', '"');
					$this->setUrl($this->siteurl);
					
					$this->headline = $this->getJson("h1[@class='title-big-detail']/text()", true);
					$this->img = $this->getJson("div[@class='img-wrap']/img/@src", true);
					$this->body = $this->getJson("div[@class='detail-content']",true);
					$this->categories = $this->getBetween($this->siteurl,"http://m.",".");
					$this->date = $this->getJson("span[@class='meta-author']/span[3]/text()", true);
					$this->author = $this->getJson("span[@class='meta-author']/span[1]/text()", true);
					$this->method_callback($this->publisher);			
				
					if($this->headline && $this->body):
						$a->{$i}		= $this->json;
					endif;

					$i++;
				endif;
			}
		}
		$this->return = $a;
		return true;
	}

	public function detail_article(){
		//get all meta tags
		$this->getMetaTags();

		switch($this->source):
			case 'Liputan6':
				$this->img 			= $this->getBetween($this->img, 'data-image="', '"');
				$this->author 		= $this->removeNewlines($this->author);
			break;

			case 'Detik':
				$this->body 	= $this->deleteBetween($this->body, '<div class="paging_artikel">', '</div>');
				$this->body 	= $this->deleteBetween($this->body, '<div class="paging_artikel_bar">', '</div></div>');
				$this->body 	= $this->deleteBetween($this->body, '<div class="label_fokus">', '</div></div>');
				$this->img		= $this->getBetween($this->img, 'src="', '"');
			break;

			case 'Kompas':
				$this->img 		= $this->getBetween($this->img, 'src="', '"');
			break;

			case 'Viva':
				$this->headline = $this->removeNewlines($this->headline);
				$this->date 	= $this->removeNewlines($this->date);
				$this->author 	= $this->removeNewlines($this->author);
				$this->img		= $this->getBetween($this->img, 'src="', '"');
				$this->body 	= $this->removeNewlines($this->body);
				$this->body 	= '<strong>'.$this->getBetween($this->body, "<strong>", '<div id="readmore">');
				$this->body 	= $this->deleteBetween($this->body, '<ul class="base-list">', '</ul>');
			break;

		endswitch;

		$this->body = strip_tags($this->body, '<p><br><strong><h6><img><ul><li><ol>');

		$this->json = (object)array(
				'headline' 		=> $this->headline,
				'date'			=> $this->date,
				'img'			=> $this->img,
				'categories'	=> $this->categories,
				'siteurl'		=> $this->siteurl,
				'body' 			=> $this->body,
				'author'		=> $this->author,
				'source'		=> $this->source,
				'meta'			=> $this->meta,
			);
	}

}