<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

include 'Content.lib.php';

$c = New Content();
$data = (object)array();

//active this line if just debug
$c->setLimit(5);

//available publisher
// Kompas,Detik,Liputan6,Merdeka

//get kompas
#$c->setPublisher('Kompas');
#$data->kompas = $c->return;

//get detik
#$c->setPublisher('Detik');
#$data->detik = $c->return;

//get liputan6
#$c->setPublisher('Liputan6');
#$data->liputan6 = $c->return;

//get merdeka
#$c->setPublisher('Merdeka');
#$data->merdeka = $c->return;

//get Viva
$c->setPublisher('Viva');
$data->viva = $c->return;

$c->debugArray($data);
?>